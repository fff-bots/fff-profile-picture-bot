✅ Dein Profilbild wurde generiert!
Du kannst es jetzt *herunterladen und als Profilbild auf
Telegram, WhatsApp, Instagram, Twitter oder ähnlichem Nutzen!*
Bitte *teile* diesen Bot mit deinen
FreundInnen oder sende mir ein Profilbild als Bild (nicht als Datei!) und ich generiere ein
Profilbild daraus!
Die Designs stammen von der offiziellen Website fridaysforfuture.de

#fridaysforfuture
💚🏳️‍🌈🌎💪

Sende /help für Informationen zu diesem Bot und Datenschutz.