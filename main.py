# !/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import os
import uuid

from PIL import Image
from telegram.ext import Updater, MessageHandler, Filters, CommandHandler
from fff_profile_picture import Generator

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


with open("strings/done.md", 'r') as f:
    done_text = f.read()
with open("strings/help.md", 'r') as f:
    help_text = f.read()
with open("strings/no_image_found.md", 'r') as f:
    no_image_found_text = f.read()


def process_image(background, update):
    status = update.message.reply_text("🔵 Startvorgang")

    filename_bare = f"{uuid.uuid1()}.png"
    result_filename = f"working/result{filename_bare}"
    status.edit_text("🔃 Rendering.")
    generator = Generator(background)
    result = generator.process()
    result.save(result_filename)
    status.edit_text("🔃 Rendering..")
    with open(result_filename, "rb") as file:
        status.edit_text("✈️ Wird gesendet...")
        update.message.reply_photo(file)
    os.remove(result_filename)
    status.edit_text(done_text, parse_mode="Markdown")


def start(update, context):
    print("Used bot with profile picture")
    user = update.effective_user
    if len(user.get_profile_photos()["photos"]) == 0:
        update.message.reply_text(no_image_found_text, parse_mode="Markdown")
        return
    photo_id = user.get_profile_photos()["photos"][0][-1]["file_id"]
    filename_bare = f"{uuid.uuid1()}.png"
    filename = f"working/{filename_bare}"
    new_file = context.bot.getFile(photo_id)
    new_file.download(filename)
    background = Image.open(filename)
    os.remove(filename)
    process_image(background, update)


def bot_help(update, context):
    update.message.reply_text(help_text, parse_mode="Markdown")


def image_handler(update, context):
    print("Used Bot with foreign image")
    filename_bare = f"{uuid.uuid1()}.png"
    filename = f"working/{filename_bare}"
    photo_file = update.message.photo[-1].get_file()
    photo_file.download(filename)
    background = Image.open(filename)
    os.remove(filename)

    process_image(background, update)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update["update_id"], context.error)


def main():
    """Start the bot."""
    updater = Updater(os.environ.get("TOKEN"), use_context=True)

    dp = updater.dispatcher
    dp.add_handler(CommandHandler("help", bot_help))
    dp.add_handler(MessageHandler(Filters.text, start))
    dp.add_handler(MessageHandler(Filters.photo, image_handler))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    main()
